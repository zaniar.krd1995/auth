import { IsEmail, IsNotEmpty, Length, Matches } from "class-validator";

export class SigninDTO {
  @IsNotEmpty({message: 'NOT_EMPTY'})
  @IsEmail({message: 'INVALID_EMAIL'})
  email: string;

  @IsNotEmpty({message: 'NOT_EMPTY'})
  @Length(8, 24, { message: 'WRONG_LENGTH'})
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {message: 'PASSWORD_WEAK'})
  password: string;
}

