import { IsNotEmpty, IsString, Length, Matches } from "class-validator";
import { Match } from "../../general/decorator/match.decorator";
import { SigninDTO } from "./signin.dto";

export class SignupDTO extends SigninDTO {
  @IsNotEmpty({message: 'NOT_EMPTY'})
  @IsString({message: 'IS_STRING'})
  @Length(8, 24, { message: 'INVALID_LENGTH'})
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {message: 'PASSWORD_WEAK'})
  @Match('password', {message: 'PASSWORD_MISMATCH'})
  passwordConfirm: string;
}