import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { TypeOrmExModule } from '../general/typeorm-ex.module';
import { UsersRepository } from './database/user.repository';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from '../general/jwt.strategy';
require('dotenv').config({path: `.env.${process.env.NODE_ENV}`});

@Module({
  imports: [
    TypeOrmExModule.forCustomRepository([UsersRepository]),
    JwtModule.register({
      secret: process.env.SECRET,
      signOptions: { expiresIn: `${process.env.TOKEN_EXPIRATION_TIME}` }
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy]
})
export class AuthModule {}
