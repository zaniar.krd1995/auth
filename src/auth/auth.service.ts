import { HttpException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersRepository } from './database/user.repository';
import { SignupDTO } from './dto/signup.dto';
import * as bcrypt from 'bcrypt';
import { User } from './database/user.entity';
import { SigninDTO } from './dto/signin.dto';

// FIXME Replace http exception with grpc exception

@Injectable()
export class AuthService {

  constructor(
    private readonly repository: UsersRepository,
    private readonly jwtService: JwtService,
  ) {}


  async signup(dto: SignupDTO): Promise<string> {
    dto.password = await this.hashPasswordWithSalt(dto.password);
    const newUser = await this.create(dto);
    return this.sign(newUser.id, newUser.roles);
  }


  private async create(dto: SignupDTO): Promise<User> {
    const user = await this.repository.findByEmail(dto.email);
    if (user) throw new HttpException('user already exists', 409);

    const newUser = this.repository.create(dto);
    return this.repository.save(newUser);
  }


  private async hashPasswordWithSalt(password: string): Promise<string> {
    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(password, salt);
    return hashedPassword;
  }


  private sign(id: string, roles: string[]): string {
    return this.jwtService.sign({sub: id, roles});
  }


  async signin(dto: SigninDTO): Promise<string> {
    const user = await this.repository.findByEmail(dto.email);
    if (!user) throw new HttpException('user not found', 404);
    
    const isPasswordMatched = await bcrypt.compare(dto.password, user.password);
    if (!isPasswordMatched) throw new HttpException('user not found', 404);
    return this.sign(user.id, user.roles);
  }

}
