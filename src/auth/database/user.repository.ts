import { Repository } from "typeorm";
import { CustomRepository } from "../../general/decorator/typeorm-ex.decorator";
import { User } from "./user.entity";

@CustomRepository(User)
export class UsersRepository extends Repository<User> {

  async findByEmail(email: string): Promise<User> {
    return this.findOneBy({ email });
  }


  async findById(id: string): Promise<User> {
    return this.findOneBy({id});
  }

}