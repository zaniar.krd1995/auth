import { Column, Entity, Index } from "typeorm";
import { Exclude } from "class-transformer";
import { BaseEntity } from "../../general/base.entity";
import { RoleType } from "../../general/enum/role.enum";

@Entity({ name: 'users' })
export class User extends BaseEntity {

  @Column({ nullable: true })
  name: string;

  @Column({
    type: 'enum',
    array: true,
    enum: RoleType,
    default: [RoleType.CUSTOMER]
  })
  roles: RoleType[];

  @Index({ unique: true })
  @Column({ nullable: false })
  email: string;

  @Exclude({ toPlainOnly: true })
  @Column({ nullable: false })
  password: string;

  @Column({ nullable: true })
  avatar: string;
}