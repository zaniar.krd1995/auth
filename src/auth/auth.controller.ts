import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { SigninDTO } from './dto/signin.dto';
import { SignupDTO } from './dto/signup.dto';

@Controller('auth')
export class AuthController {

  constructor(private readonly service: AuthService) {}


  @Post('signup')
  async signup(@Body() dto: SignupDTO) {
    return { 
      token: await this.service.signup(dto)
    }
  }


  @Post('signin')
  async signin(@Body() dto: SigninDTO) {
    return {
      token: await this.service.signin(dto)
    }
  }

}
