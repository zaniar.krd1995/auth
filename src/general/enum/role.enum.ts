export enum RoleType {
  HOTELIER = 'hotelier',
  CUSTOMER = 'customer',
}