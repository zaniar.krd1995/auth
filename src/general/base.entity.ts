import { Exclude } from "class-transformer";
import { CreateDateColumn, DeleteDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

export abstract class BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn({type: 'timestamptz', name: 'created_date'})
  createdDate: Date;

  @UpdateDateColumn({type: 'timestamptz', name: 'updated_date'})
  updatedDate: Date;

  @Exclude({toPlainOnly: true})
  @DeleteDateColumn({type: 'timestamptz', name: 'deleted_date' })
  deletedAt: Date;
}